import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { Opcion1Component} from './opcion1/opcion1.component';
import { Opcion2Component } from './opcion2/opcion2.component';
import { Opcion3Component } from './opcion3/opcion3.component';
import { Opcion4Component } from './opcion4/opcion4.component';
import { Opcion5Component } from './opcion5/opcion5.component';
import { OpcionnotfoundComponent } from './opcionnotfound/opcionnotfound.component';
import { CreditosComponent } from './creditos/creditos.component';


const routes: Routes = [
  {
    path: '', redirectTo: 'creditos', pathMatch: 'full'
  },
 
  {
   path: 'creditos', component: CreditosComponent
 },
 {
   path: 'opcion1', component: Opcion1Component
 },
 {
   path: 'opcion2', component: Opcion2Component
 },
 {
   path: 'opcion3', component: Opcion3Component
 },
 {
   path: 'opcion4', component: Opcion4Component
 },
 {
   path: 'opcion5', component: Opcion5Component
 },
 {
  path: '**', component: OpcionnotfoundComponent
 }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
