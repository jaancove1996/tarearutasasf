import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpcionnotfoundComponent } from './opcionnotfound.component';

describe('OpcionnotfoundComponent', () => {
  let component: OpcionnotfoundComponent;
  let fixture: ComponentFixture<OpcionnotfoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpcionnotfoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpcionnotfoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
