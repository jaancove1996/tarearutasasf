import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Opcion1Component } from './opcion1/opcion1.component';
import { Opcion2Component } from './opcion2/opcion2.component';
import { Opcion3Component } from './opcion3/opcion3.component';
import { Opcion4Component } from './opcion4/opcion4.component';
import { Opcion5Component } from './opcion5/opcion5.component';
import { OpcionnotfoundComponent } from './opcionnotfound/opcionnotfound.component';
import { CreditosComponent } from './creditos/creditos.component';

@NgModule({
  declarations: [
    AppComponent,
    Opcion1Component,
    Opcion2Component,
    Opcion3Component,
    Opcion4Component,
    Opcion5Component,
    OpcionnotfoundComponent,
    CreditosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
